const initialState = {
  events: {},
  eventIds: [],
  markets: {},
  selections: {},
  betIds: [],
  sidebarIsOpen: false
};

const globalReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'global/GET_EVENTS':
      return Object.assign({}, state, {
        events: action.payload.events,
        eventIds: action.payload.eventIds,
        markets: action.payload.markets,
        selections: action.payload.selections,
      });
    case 'global/TOGGLE_SIDEBAR':
      return Object.assign({}, state, {
        sidebarIsOpen: action.payload.sidebarIsOpen
      });
    case 'global/SELECT_BET':
      return Object.assign({}, state, {
        betIds: action.payload.betIds
      });
    case 'global/DELETE_BET':
      return Object.assign({}, state, {
        betIds: action.payload.betIds
      });

    /* no default */

  }
  return state;
};

export default globalReducers;
