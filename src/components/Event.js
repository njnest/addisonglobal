import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './Event.css';

import Market from './Market';

const Event = ({ id, events }) => (
  <div className="c-event">
    <span className="c-event__name">
      {events[id].name}
    </span>
    {
      events[id].marketIds.map(id => (
        <Market key={id} id={id} />
      ))
    }
  </div>
);

Event.propTypes = {
  events: PropTypes.object,
  id: PropTypes.string.isRequired
};

export default connect(store => ({
  events: store.global.events
}))(Event);
