import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './EventList.css';

import Event from './Event';

const EventList = ({ eventIds, events }) => (
  <div className="c-event-list">
    {
      eventIds.map(id => events[id].marketIds.length > 0 ? (
        <Event id={id} key={id}/>
      ) : null)
    }
  </div>
);

EventList.propTypes = {
  eventIds: PropTypes.array,
  events: PropTypes.object
};

export default connect(store => ({
  eventIds: store.global.eventIds,
  events: store.global.events
}))(EventList);
