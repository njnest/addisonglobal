import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Header.css';
import { toggleSidebar } from '../actions/globalActions';

class Header extends Component {
  constructor(props) {
    super(props);
    this.toggleSidebar = this.toggleSidebar.bind(this);
  }
  toggleSidebar() {
    this.props.dispatch(toggleSidebar(this.props.sidebarIsOpen));
  }
  render() {
    return (
      <header className="c-header">
        <div className="c-close-btn-wrap">
          <a className={classNames("c-close-btn", {
            "c-close-btn--selected": this.props.sidebarIsOpen
          })} onClick={this.toggleSidebar}>
            <div className="bar1"></div>
            <div className="bar2"></div>
            <div className="bar3"></div>
          </a>
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  dispatch: PropTypes.func,
  sidebarIsOpen: PropTypes.bool
};

export default connect(store => ({
  sidebarIsOpen: store.global.sidebarIsOpen
}))(Header);
