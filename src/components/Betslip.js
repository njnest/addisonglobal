import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Betslip.css';

import { deleteBet } from '../actions/globalActions';


class Betslip extends Component {
  constructor(props) {
    super(props);
    this.deleteBet = this.deleteBet.bind(this);
  }
  deleteBet(betId) {
    this.props.dispatch(deleteBet({betId}));
  }
  render() {
    return (
      <div className={classNames("c-betslip", {
        "c-betslip__is-open": this.props.sidebarIsOpen
      })}>
        <div className="c-betslip-list-wrap">
          <div className="c-betslip-list">
            {
              this.props.betIds.map(id => (
                <div className="c-bet" key={id}>
                  <span className="c-bet__name">
                    {this.props.selections[id].name} ({this.props.selections[id].marketType})
                  </span>
                  <span className="c-bet__price">
                    {this.props.selections[id].price}
                  </span>
                  <a className="c-btn" onClick={this.deleteBet.bind(this, id)}>
                    delete
                  </a>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

Betslip.propTypes = {
  dispatch: PropTypes.func,
  betIds: PropTypes.array,
  selections: PropTypes.object,
  sidebarIsOpen: PropTypes.bool,
};

export default connect(store => ({
  betIds: store.global.betIds,
  selections: store.global.selections,
  sidebarIsOpen: store.global.sidebarIsOpen
}))(Betslip);
