import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Market.css';

import { selectBet } from '../actions/globalActions';

class Market extends Component {
  constructor(props) {
    super(props);
    this.selectBet = this.selectBet.bind(this);
  }
  selectBet(id) {
    this.props.dispatch(selectBet({id}));
  }
  render() {
    const { id, markets, selections, betIds } = this.props;
    return (
      <div className="c-market">
        <span className="c-market__name">
          {markets[id].name}
        </span>
        <div className="c-market__selections">
          {
            markets[id].selectionIds.map(id => (
              <a className={classNames("c-market__selection", {
                "c-market__selection--selected": betIds.includes(id)
              })} onClick={this.selectBet.bind(this, id)} key={id}>
                <span className="c-market__selection-name">
                  {selections[id].name}
                </span>
                <span className="c-market__selection-price">
                  {selections[id].price}
                </span>
              </a>
            ))
          }
        </div>
      </div>
    );
  }
}

Market.propTypes = {
  dispatch: PropTypes.func,
  markets: PropTypes.object,
  selections: PropTypes.object,
  betIds: PropTypes.array,
  id: PropTypes.string.isRequired
};

export default connect(store => ({
  markets: store.global.markets,
  selections: store.global.selections,
  betIds: store.global.betIds
}))(Market);
