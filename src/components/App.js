import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './App.css';

import { getEventList } from '../actions/globalActions';
import Header from './Header';
import EventList from './EventList';
import Betslip from './Betslip';


class App extends Component {
  componentDidMount() {
    this.props.dispatch(getEventList());
  }
  render() {
    return (
      <div className="App">
        <Header />
        <EventList />
        <Betslip />
      </div>
    );
  }
}

App.propTypes = {
  dispatch: PropTypes.func
};

export default connect()(App);
