import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import reducers from '../reducers';

import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

const logger = createLogger();

const composeEnhancers = composeWithDevTools({
  maxAge: 15,
  latency: 1500,
});
export default createStore(reducers, composeEnhancers(
  applyMiddleware(promise(), thunk, logger),
));
