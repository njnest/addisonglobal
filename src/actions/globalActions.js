import axios from 'axios';

export function getEventList() {
  return dispatch => {
    axios.get('http://www.mocky.io/v2/59f08692310000b4130e9f71').then(function (response) {
      let events = {};
      let markets = {};
      let selections = {};
      const eventIds = response.data.map(e => {
        events[e.id] = e;
        e["marketIds"] = e.markets.map(m => {
          markets[m.id] = m;
          m["selectionIds"] = m.selections.map(s => {
            s["marketType"] = m.name;
            selections[s.id] = s;
            return s.id;
          });
          return m.id;
        });
        return e.id;
      });

      dispatch({
        type: 'global/GET_EVENTS',
        payload: {
          events,
          eventIds,
          markets,
          selections,
        }
      });
    }).catch(function (error) {
      dispatch({
        type: 'global/GET_EVENTS_ERR',
        payload: {
          error
        }
      });
    });
  };
}

export function selectBet({ id }) {
  return (dispatch, getState) => {
    const { betIds } = getState().global;
    let changedBetIds = [];
    if (betIds.includes(id)) {
      changedBetIds = betIds.filter(bId => bId !== id);
    } else {
      changedBetIds = betIds.concat([id]);
    }

    dispatch({
      type: 'global/SELECT_BET',
      payload: {
        betIds: changedBetIds
      }
    });
  };
}

export function deleteBet({ betId }) {
  return (dispatch, getState) => {
    const { betIds } = getState().global;
    dispatch({
      type: 'global/DELETE_BET',
      payload: {
        betIds: betIds.filter(id => id !== betId)
      }
    });
  };
}

export function toggleSidebar(sidebarIsOpen) {
  return {
    type: 'global/TOGGLE_SIDEBAR',
    payload: {
      sidebarIsOpen: !sidebarIsOpen
    }
  };
}
